
## Installation

1. Créez une base de données et exécutez le script **database.sql**
2. Copiez le script **configuration.sample.php** vers **configuration.php**
3. Éditez les valeurs de ce nouveau script
4. Lancez *à la racine du projet* la commande suivante :

```
php -S 0.0.0.0:8000
```

5. Visitez l'adresse suivant : http://localhost:8000


## Avertissement

Ceci n'est **pas** une correction ! Si vous avez fait autrement, c'est sûrement bon aussi. C'est l'expression du besoin / la demande client / le cahier des charges, qui fait office de loi ;)


## Explication de texte

Ce code a été fait de sorte qu'une seule page HTML existe. Sur cette page, il est possible de réaliser plein d'actions : ajouter, modifier, supprimer des projets, utilisateurs, etc... À  chaque fois, cela est possible via un formulaire, qui contient systématiquement un champ caché INPUT nommé **action**. C'est le fichier **router.php** qui traite ces actions.

Ce fichier exécute l'action en faisant appel à des outils : **participation.php**, **project.php** et **user.php**. Ces outils sont des fonctions qui permettent de sélectionner, ajouter, modifier et supprimer des données de la BDD (pensez CRUD). Cela n'est fait que pour une chose : simplifier la lecture du code !

